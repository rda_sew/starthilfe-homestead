<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Starthelfer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route(
     *     path="/",
     *     name="home",
     *     options={"sitemap" = true}
     * )
     * @return Response
     */
    public function home(): Response
    {
        $test = $this->getDoctrine()->getRepository(Starthelfer::class)->findAll();

        $starthelfers = $this->get('serializer')->serialize($test, 'json');
        dump($starthelfers);

        return $this->render('home.html.twig', compact('starthelfers'));
    }
}
