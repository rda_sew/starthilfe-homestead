<?php

namespace App\Repository;

use App\Entity\Starthelfer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Starthelfer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Starthelfer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Starthelfer[]    findAll()
 * @method Starthelfer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StarthelferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Starthelfer::class);
    }

    // /**
    //  * @return Starthelfer[] Returns an array of Starthelfer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Starthelfer
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
